package com.joncasagrande.wechannel.model

import android.content.Context
import android.view.View


interface WeChannel{
    fun playChannel(context: Context) : View?
    fun getChannelDisplay(): Int
    fun getChannelId() : String
    fun getChannelName() : String
}