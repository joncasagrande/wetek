package com.joncasagrande.wechannel.model

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.Glide

class Player2(val id:String, val name: String,val  thumbnail: Int) : WeChannel{

    override fun getChannelId(): String = id

    override fun getChannelDisplay(): Int = thumbnail

    override fun getChannelName(): String = name

    override fun playChannel(context: Context): View? {
        val edtParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        edtParams.setMargins(16, 4, 16, 16)
        val imageView = ImageView(context)
        Glide.with(context).load(thumbnail).into(imageView)
        imageView.layoutParams = edtParams
        return imageView
    }

}