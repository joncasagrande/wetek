package com.joncasagrande.wechannel.model

import android.content.Context
import android.view.View
import android.widget.TextView
import android.widget.LinearLayout





class Player1(val id:String,val  name: String, val thumbnail: Int) : WeChannel{

    override fun playChannel(context: Context): View? {
        val edtParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        edtParams.setMargins(16, 4, 16, 16)
        val textView = TextView(context)
        textView.setText("CHANNEL 1 IS DISPLAYING SOME STREAMING OF STREAM OR STREAMING A STREAM STREAMING")
        textView.layoutParams = edtParams


        return textView
    }

    override fun getChannelId(): String = id

    override fun getChannelDisplay(): Int = thumbnail
    override fun getChannelName(): String = name
}