package com.joncasagrande.wechannel

import com.joncasagrande.wechannel.model.WeChannel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe



class MainActivityPresenter(val presenter: Presenter){
    interface Presenter{
        fun playChannel( weChannel: WeChannel)
    }
    init {
        EventBus.getDefault().register(this)
    }


    fun setPlayerOnView(weChannel: WeChannel){
        presenter.playChannel(weChannel)
    }

    @Subscribe
    fun onMessageEvent(event: WeChannel) {
        setPlayerOnView(event)
    }

    fun destroy(){
        EventBus.getDefault().unregister(this)
    }

}