package com.joncasagrande.wechannel

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.joncasagrande.wechannel.model.Player1
import com.joncasagrande.wechannel.model.Player2
import com.joncasagrande.wechannel.model.WeChannel
import com.joncasagrande.wechannel.recyclerView.ChannelRecyclerView
import com.joncasagrande.wechannel.util.channel1
import com.joncasagrande.wechannel.util.channel2
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v7.widget.DividerItemDecoration


class MainActivity : AppCompatActivity(),MainActivityPresenter.Presenter {


    lateinit var channelRecyclerViewAdapter : ChannelRecyclerView
    lateinit var mainActivityViewModel :MainActivityPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainActivityViewModel =MainActivityPresenter(this)
        channelRecyclerViewAdapter = ChannelRecyclerView(this,
            arrayListOf(Player1(channel1,"Channel 1",R.drawable.channel1), Player2(channel2,"Channel 2",R.drawable.channel2)) as List<out WeChannel>
        )
        val channelLayout = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        val dividerItemDecoration = DividerItemDecoration(
            this,
            channelLayout.getOrientation()
        )
        RVchannel.addItemDecoration(dividerItemDecoration)

        RVchannel.layoutManager = channelLayout
        RVchannel.adapter = channelRecyclerViewAdapter

    }

    override fun playChannel(weChannel: WeChannel) {
        hideChannelList()

        TVchannelName.text = weChannel.getChannelName()


        channelPlayer.removeAllViews()

        channelPlayer.addView(weChannel.playChannel(this))

    }
    fun showChannels(view: View ){
        RVchannel.animate().translationY(0f)
        RVchannel.visibility = View.VISIBLE


        Handler().postDelayed({
                if(RVchannel.visibility == View.VISIBLE){
                    hideChannelList()
                }

        }, 2000)
    }
    private  fun hideChannelList(){
        RVchannel.animate().translationY(-75f)
        RVchannel.visibility = View.INVISIBLE
        TVchannelName.visibility = View.VISIBLE
        channelPlayer.visibility  = View.VISIBLE
        IBback.visibility  = View.VISIBLE
    }

}
